# Worklog del proyecto DMC

El proyecto consta en implementar una estructura de contenedores que sirvan ciertas aplicaciones y que entre ellas compartan autenticación LDAP. El desarrollo de este proyecto se ha dividido en las siguientes ventanas temporales:

## Semana 0 (16-22 abril)
Día | Objetivo | Conseguido
--  | --       | --
16 | |
17 | Estructuración del worklog del proyecto y primera búsqueda de información sobre herramientas y tecnologías a utilizar. | DONE
18 | OverdriveCON | MISS
19 | OverdriveCON | MISS
20 | OverdriveCON | MISS
21 | WEEKEND | FREE
22 | WEEKEND | FREE

## Semana 1 (23-29 abril)
Día | Objetivo | Conseguido
--  | --       | --
23 |  <ul><li>Primera entrevista con Jordi Andugar para plantear la dirección del proyecto.</li><li>Investigación de el proceso de indexación que sigue Nextcloud y PLEX.</li></ul> |
24 | <ul><li>Estructuración del trabajo en apartados<li>Investigación y documentación sobre alternativas para PLEX</li></ul> | DONE
25 | <ul><li>Investigación y documentación sobre alternativas para PLEX</li><li>Prueba de software de streaming audio y video + MySQL</li></ul> | DONE
26 | <ul><li>Prueba de estrés de Ampache en contenedores</li></ul> | DONE
27 | <ul><li>Puesta en pruebas de la autenticación de LDAP con Nextcloud.</li><li>Revisión y ampliación de documentación.</li></ul> | DONE
28 | WEEKEND | DONE
29 | WEEKEND | DONE

## Semana 2 (30-06 mayo)
Día | Objetivo | Conseguido
--  | --       | --
30 | FESTIVO | DONE
01 | FESTIVO | DONE
02 | <ul><li>Puesta en pruebas de la autenticación de LDAP con Nextcloud.</li><li>Revisión y ampliación de documentación.</li></ul> | DONE
03 | <ul><li>Automatizar arranque de contenedor Nextcloud.</li><li>Automatizar arranque de contenedor LDAP</li><li>Automatizar arranque de contenedor MySQL.</li></ul> | DONE
04 | Solución de problemas con la exportación/importación de la BBDD de Nextcloud y documentación. | DONE
05 | Solución de problemas con la exportación/importación de la BBDD de Nextcloud y documentación. | DONE
06 | Solución de problemas con docker-compose y documentación. |

## Semana 3 (07-13 mayo)
Día | Objetivo | Conseguido
--  | --       | --
07 | Investigación y entorno de pruebas de Ampache y documentación. | DONE
08 | Pruebas de tematización, configuración de LDAP y catálogos de Ampache + documentación. | DONE
09 | <ul><li>Investigación del protocolo de streaming utilizado por Ampache.</li><li>Construcción del contenedor final y testeo de este para su correcto funcionamiento.</li></ul> | DONE
10 | <ul><li>Automatización del arrancado de la infraestructura añadiendo el contenedor de Ampache y solución de los problemas encontrados.</li><li>Puesta a punto de la documentación.</li></ul>| DONE
11 | <ul><li>Ampliar documentación</li><li>Update con Eduard Canet</li><li>Solucionar problema trusted_domains</ul></ul> | DONE
12 | WEEKEND | DONE
13 | WEEKEND | DONE

## Semana 4 (14-20 mayo)
Día | Objetivo | Conseguido
--  | --       | --
14 | <ul><li>Añadir módulos a nextcloud.</li><li>Añadir radios y música a ampache.</li><li>Rehacer dumps de nextcloud y ampache.</li><li>Ampliar documentación.</li><li>Solucinar bug carpeta temporales.</li></ul> | DONE
15 | <ul><li>Restructurar documentación</li><li>Investigar broadcast de Ampache</li> | DONE
16 | Poner en práctica los broadcast de Ampache | DONE
17 | Resolver problemas con los broadcast de Ampache | DONE
18 | <ul><li>Hacer la presentación del proyecto</li><li>Ampliar documentación.</li> | DONE
19 | WEEKEND | DONE
20 | Repasar status del funcionamiento del proyecto y de la documentación. | DONE
