-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Linux (x86_64)
--
-- Host: 172.10.10.2    Database: ampache
-- ------------------------------------------------------
-- Server version	5.6.40

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE ampache;
USE ampache;

--
-- Table structure for table `access_list`
--

DROP TABLE IF EXISTS `access_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `start` varbinary(255) NOT NULL,
  `end` varbinary(255) NOT NULL,
  `level` smallint(3) unsigned NOT NULL DEFAULT '5',
  `type` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `user` int(11) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `start` (`start`),
  KEY `end` (`end`),
  KEY `level` (`level`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_list`
--

LOCK TABLES `access_list` WRITE;
/*!40000 ALTER TABLE `access_list` DISABLE KEYS */;
INSERT INTO `access_list` VALUES (1,'DEFAULTv4','\0\0\0\0','����',75,'interface',-1,1),(2,'DEFAULTv4','\0\0\0\0','����',75,'stream',-1,1),(3,'DEFAULTv4','\0\0\0\0','����',75,'rpc',-1,1),(4,'DEFAULTv6','\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','��������',75,'interface',-1,1),(5,'DEFAULTv6','\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','��������',75,'stream',-1,1),(6,'DEFAULTv6','\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','��������',75,'rpc',-1,1);
/*!40000 ALTER TABLE `access_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `album`
--

DROP TABLE IF EXISTS `album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `album` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `prefix` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `mbid` varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  `year` int(4) unsigned NOT NULL DEFAULT '1984',
  `disk` smallint(5) unsigned DEFAULT NULL,
  `mbid_group` varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  `release_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `album_artist` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `year` (`year`),
  KEY `disk` (`disk`),
  FULLTEXT KEY `name_2` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `album`
--

LOCK TABLES `album` WRITE;
/*!40000 ALTER TABLE `album` DISABLE KEYS */;
INSERT INTO `album` VALUES (1,'Desconocido (Huerfano)',NULL,NULL,0,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artist`
--

DROP TABLE IF EXISTS `artist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `prefix` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `mbid` varchar(1369) CHARACTER SET utf8 DEFAULT NULL,
  `summary` text CHARACTER SET utf8,
  `placeformed` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `yearformed` int(4) DEFAULT NULL,
  `last_update` int(11) unsigned NOT NULL DEFAULT '0',
  `user` int(11) DEFAULT NULL,
  `manual_update` smallint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  FULLTEXT KEY `name_2` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artist`
--

LOCK TABLES `artist` WRITE;
/*!40000 ALTER TABLE `artist` DISABLE KEYS */;
INSERT INTO `artist` VALUES (1,'Hel.Razor',NULL,NULL,NULL,NULL,NULL,0,NULL,0),(2,'Desconocido (Huerfano)',NULL,NULL,NULL,NULL,NULL,0,NULL,0);
/*!40000 ALTER TABLE `artist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookmark`
--

DROP TABLE IF EXISTS `bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookmark` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `position` int(11) unsigned NOT NULL DEFAULT '0',
  `comment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `object_type` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `creation_date` int(11) unsigned NOT NULL DEFAULT '0',
  `update_date` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookmark`
--

LOCK TABLES `bookmark` WRITE;
/*!40000 ALTER TABLE `bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `broadcast`
--

DROP TABLE IF EXISTS `broadcast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `broadcast` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) unsigned NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `is_private` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `song` int(11) unsigned NOT NULL DEFAULT '0',
  `started` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listeners` int(11) unsigned NOT NULL DEFAULT '0',
  `key` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `broadcast`
--

LOCK TABLES `broadcast` WRITE;
/*!40000 ALTER TABLE `broadcast` DISABLE KEYS */;
/*!40000 ALTER TABLE `broadcast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `catalog_type` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `last_update` int(11) unsigned NOT NULL DEFAULT '0',
  `last_clean` int(11) unsigned DEFAULT NULL,
  `last_add` int(11) unsigned NOT NULL DEFAULT '0',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `rename_pattern` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort_pattern` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `gather_types` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog` VALUES (1,'Videos','local',0,NULL,1525852638,1,'%T - %t','%a/%A','personal_video'),(2,'Audio','local',0,NULL,1526281785,1,'%T - %t','%a/%A','music');
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_local`
--

DROP TABLE IF EXISTS `catalog_local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_local` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `catalog_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_local`
--

LOCK TABLES `catalog_local` WRITE;
/*!40000 ALTER TABLE `catalog_local` DISABLE KEYS */;
INSERT INTO `catalog_local` VALUES (1,'/media/video/personales',1),(2,'/media/audio',2);
/*!40000 ALTER TABLE `catalog_local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_remote`
--

DROP TABLE IF EXISTS `catalog_remote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_remote` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `catalog_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_remote`
--

LOCK TABLES `catalog_remote` WRITE;
/*!40000 ALTER TABLE `catalog_remote` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_remote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `url` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `interface` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `port` int(11) unsigned NOT NULL DEFAULT '0',
  `fixed_endpoint` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `object_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `is_private` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `random` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `loop` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admin_password` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `start_date` int(11) unsigned NOT NULL DEFAULT '0',
  `max_listeners` int(11) unsigned NOT NULL DEFAULT '0',
  `peak_listeners` int(11) unsigned NOT NULL DEFAULT '0',
  `listeners` int(11) unsigned NOT NULL DEFAULT '0',
  `connections` int(11) unsigned NOT NULL DEFAULT '0',
  `stream_type` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `bitrate` int(11) unsigned NOT NULL DEFAULT '128',
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel`
--

LOCK TABLES `channel` WRITE;
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clip`
--

DROP TABLE IF EXISTS `clip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clip` (
  `id` int(11) unsigned NOT NULL,
  `artist` int(11) DEFAULT NULL,
  `song` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clip`
--

LOCK TABLES `clip` WRITE;
/*!40000 ALTER TABLE `clip` DISABLE KEYS */;
/*!40000 ALTER TABLE `clip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daap_session`
--

DROP TABLE IF EXISTS `daap_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daap_session` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `creationdate` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daap_session`
--

LOCK TABLES `daap_session` WRITE;
/*!40000 ALTER TABLE `daap_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `daap_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `democratic`
--

DROP TABLE IF EXISTS `democratic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `democratic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `cooldown` tinyint(4) unsigned DEFAULT NULL,
  `level` tinyint(4) unsigned NOT NULL DEFAULT '25',
  `user` int(11) NOT NULL,
  `primary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `base_playlist` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `primary_2` (`primary`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `democratic`
--

LOCK TABLES `democratic` WRITE;
/*!40000 ALTER TABLE `democratic` DISABLE KEYS */;
/*!40000 ALTER TABLE `democratic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image` mediumblob,
  `width` int(4) unsigned DEFAULT '0',
  `height` int(4) unsigned DEFAULT '0',
  `mime` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `size` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `object_type` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `kind` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `object_type` (`object_type`),
  KEY `object_id` (`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ip_history`
--

DROP TABLE IF EXISTS `ip_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `ip` varbinary(255) DEFAULT NULL,
  `date` int(11) unsigned NOT NULL DEFAULT '0',
  `agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`user`),
  KEY `date` (`date`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip_history`
--

LOCK TABLES `ip_history` WRITE;
/*!40000 ALTER TABLE `ip_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ip_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `label`
--

DROP TABLE IF EXISTS `label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `category` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `summary` text CHARACTER SET utf8,
  `address` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `user` int(11) unsigned DEFAULT NULL,
  `creation_date` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `label`
--

LOCK TABLES `label` WRITE;
/*!40000 ALTER TABLE `label` DISABLE KEYS */;
/*!40000 ALTER TABLE `label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `label_asso`
--

DROP TABLE IF EXISTS `label_asso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `label_asso` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `label` int(11) unsigned NOT NULL,
  `artist` int(11) unsigned NOT NULL,
  `creation_date` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `label_asso`
--

LOCK TABLES `label_asso` WRITE;
/*!40000 ALTER TABLE `label_asso` DISABLE KEYS */;
/*!40000 ALTER TABLE `label_asso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `license`
--

DROP TABLE IF EXISTS `license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `license` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `external_link` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `license`
--

LOCK TABLES `license` WRITE;
/*!40000 ALTER TABLE `license` DISABLE KEYS */;
INSERT INTO `license` VALUES (1,'_default',NULL,''),(2,'CC BY',NULL,'https://creativecommons.org/licenses/by/3.0/'),(3,'CC BY NC',NULL,'https://creativecommons.org/licenses/by-nc/3.0/'),(4,'CC BY NC ND',NULL,'https://creativecommons.org/licenses/by-nc-nd/3.0/'),(5,'CC BY NC SA',NULL,'https://creativecommons.org/licenses/by-nc-sa/3.0/'),(6,'CC BY ND',NULL,'https://creativecommons.org/licenses/by-nd/3.0/'),(7,'CC BY SA',NULL,'https://creativecommons.org/licenses/by-sa/3.0/'),(8,'Licence Art Libre',NULL,'http://artlibre.org/licence/lal/'),(9,'Yellow OpenMusic',NULL,'http://openmusic.linuxtag.org/yellow.html'),(10,'Green OpenMusic',NULL,'http://openmusic.linuxtag.org/green.html'),(11,'Gnu GPL Art',NULL,'http://gnuart.org/english/gnugpl.html'),(12,'WTFPL',NULL,'https://en.wikipedia.org/wiki/WTFPL'),(13,'FMPL',NULL,'http://www.fmpl.org/fmpl.html'),(14,'C Reaction',NULL,'http://morne.free.fr/Necktar7/creaction.htm');
/*!40000 ALTER TABLE `license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `live_stream`
--

DROP TABLE IF EXISTS `live_stream`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_stream` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `site_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `url` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `genre` int(11) unsigned NOT NULL DEFAULT '0',
  `catalog` int(11) unsigned NOT NULL DEFAULT '0',
  `codec` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog` (`catalog`),
  KEY `genre` (`genre`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `live_stream`
--

LOCK TABLES `live_stream` WRITE;
/*!40000 ALTER TABLE `live_stream` DISABLE KEYS */;
INSERT INTO `live_stream` VALUES (3,'isira','','http://www.tnltvisira.com/',0,2,'mp3'),(4,'Colombia','','http://149.56.147.197:9641/cNeiva',0,2,'mp3'),(5,'Rampa','','http://stream.radio.co/s601fe08ae/listen',0,2,'mp3');
/*!40000 ALTER TABLE `live_stream` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localplay_httpq`
--

DROP TABLE IF EXISTS `localplay_httpq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localplay_httpq` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `owner` int(11) NOT NULL,
  `host` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `port` int(11) unsigned NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `access` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localplay_httpq`
--

LOCK TABLES `localplay_httpq` WRITE;
/*!40000 ALTER TABLE `localplay_httpq` DISABLE KEYS */;
/*!40000 ALTER TABLE `localplay_httpq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localplay_mpd`
--

DROP TABLE IF EXISTS `localplay_mpd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localplay_mpd` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `owner` int(11) NOT NULL,
  `host` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `port` int(11) unsigned NOT NULL DEFAULT '6600',
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `access` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localplay_mpd`
--

LOCK TABLES `localplay_mpd` WRITE;
/*!40000 ALTER TABLE `localplay_mpd` DISABLE KEYS */;
INSERT INTO `localplay_mpd` VALUES (1,'Prueba',1,'192.168.2.59',5001,'test',0);
/*!40000 ALTER TABLE `localplay_mpd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localplay_shoutcast`
--

DROP TABLE IF EXISTS `localplay_shoutcast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localplay_shoutcast` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `owner` int(11) NOT NULL,
  `pid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `playlist` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `local_root` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `access` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localplay_shoutcast`
--

LOCK TABLES `localplay_shoutcast` WRITE;
/*!40000 ALTER TABLE `localplay_shoutcast` DISABLE KEYS */;
/*!40000 ALTER TABLE `localplay_shoutcast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metadata`
--

DROP TABLE IF EXISTS `metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` int(11) unsigned NOT NULL,
  `field` int(11) unsigned NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `field` (`field`),
  KEY `object_id` (`object_id`),
  KEY `type` (`type`),
  KEY `objecttype` (`object_id`,`type`),
  KEY `objectfield` (`object_id`,`field`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metadata`
--

LOCK TABLES `metadata` WRITE;
/*!40000 ALTER TABLE `metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metadata_field`
--

DROP TABLE IF EXISTS `metadata_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata_field` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `public` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metadata_field`
--

LOCK TABLES `metadata_field` WRITE;
/*!40000 ALTER TABLE `metadata_field` DISABLE KEYS */;
/*!40000 ALTER TABLE `metadata_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `id` int(11) unsigned NOT NULL,
  `original_name` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `summary` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `year` int(11) unsigned DEFAULT NULL,
  `prefix` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `now_playing`
--

DROP TABLE IF EXISTS `now_playing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `now_playing` (
  `id` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `object_id` int(11) unsigned NOT NULL,
  `object_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user` int(11) NOT NULL,
  `expire` int(11) unsigned NOT NULL DEFAULT '0',
  `insertion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `expire` (`expire`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `now_playing`
--

LOCK TABLES `now_playing` WRITE;
/*!40000 ALTER TABLE `now_playing` DISABLE KEYS */;
/*!40000 ALTER TABLE `now_playing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_count`
--

DROP TABLE IF EXISTS `object_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_count` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `object_type` enum('album','artist','song','playlist','genre','catalog','live_stream','video') CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) unsigned NOT NULL DEFAULT '0',
  `date` int(11) unsigned NOT NULL DEFAULT '0',
  `user` int(11) unsigned NOT NULL,
  `agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `geo_latitude` decimal(10,6) DEFAULT NULL,
  `geo_longitude` decimal(10,6) DEFAULT NULL,
  `geo_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `count_type` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `object_type` (`object_type`),
  KEY `object_id` (`object_id`),
  KEY `userid` (`user`),
  KEY `date` (`date`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_count`
--

LOCK TABLES `object_count` WRITE;
/*!40000 ALTER TABLE `object_count` DISABLE KEYS */;
INSERT INTO `object_count` VALUES (1,'video',1,1525852650,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(2,'video',1,1525852690,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(3,'video',1,1525852822,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(4,'video',1,1525852834,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(5,'video',1,1525852867,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(6,'video',1,1525852907,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(7,'video',1,1525852963,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(8,'video',1,1525858191,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(9,'video',1,1525858202,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(10,'video',1,1525858213,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(11,'video',1,1525858225,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(12,'video',1,1525858236,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(13,'video',1,1525858247,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(14,'video',1,1525858258,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(15,'video',1,1525858270,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(16,'video',1,1526283218,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(17,'video',1,1526283244,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(18,'video',1,1526283260,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(19,'video',1,1526283381,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'download'),(20,'song',4,1526286078,2,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL,'stream'),(21,'album',1,1526286078,2,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL,'stream'),(22,'artist',2,1526286078,2,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL,'stream'),(23,'video',1,1526286125,2,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL,'stream'),(24,'video',1,1526288563,1,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL,'stream'),(25,'song',2,1526288663,2,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL,'stream'),(26,'album',1,1526288663,2,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL,'stream'),(27,'artist',2,1526288663,2,'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL,'stream');
/*!40000 ALTER TABLE `object_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_video`
--

DROP TABLE IF EXISTS `personal_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_video` (
  `id` int(11) unsigned NOT NULL,
  `location` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `summary` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_video`
--

LOCK TABLES `personal_video` WRITE;
/*!40000 ALTER TABLE `personal_video` DISABLE KEYS */;
INSERT INTO `personal_video` VALUES (1,NULL,''),(2,NULL,'');
/*!40000 ALTER TABLE `personal_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_control`
--

DROP TABLE IF EXISTS `player_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_control` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) unsigned NOT NULL,
  `cmd` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `value` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `object_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `send_date` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_control`
--

LOCK TABLES `player_control` WRITE;
/*!40000 ALTER TABLE `player_control` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_control` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist`
--

DROP TABLE IF EXISTS `playlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `type` enum('private','public') CHARACTER SET utf8 DEFAULT NULL,
  `date` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist`
--

LOCK TABLES `playlist` WRITE;
/*!40000 ALTER TABLE `playlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `playlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist_data`
--

DROP TABLE IF EXISTS `playlist_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlist_data` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `playlist` int(11) unsigned NOT NULL DEFAULT '0',
  `object_id` int(11) unsigned DEFAULT NULL,
  `object_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `track` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `playlist` (`playlist`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist_data`
--

LOCK TABLES `playlist_data` WRITE;
/*!40000 ALTER TABLE `playlist_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `playlist_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `podcast`
--

DROP TABLE IF EXISTS `podcast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `podcast` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `feed` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `catalog` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `language` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `copyright` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `generator` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `lastbuilddate` int(11) unsigned NOT NULL DEFAULT '0',
  `lastsync` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `podcast`
--

LOCK TABLES `podcast` WRITE;
/*!40000 ALTER TABLE `podcast` DISABLE KEYS */;
/*!40000 ALTER TABLE `podcast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `podcast_episode`
--

DROP TABLE IF EXISTS `podcast_episode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `podcast_episode` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `guid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `podcast` int(11) NOT NULL,
  `state` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `file` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `source` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `time` smallint(5) unsigned NOT NULL DEFAULT '0',
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `author` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `category` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `played` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pubdate` int(11) unsigned NOT NULL,
  `addition_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `podcast_episode`
--

LOCK TABLES `podcast_episode` WRITE;
/*!40000 ALTER TABLE `podcast_episode` DISABLE KEYS */;
/*!40000 ALTER TABLE `podcast_episode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preference`
--

DROP TABLE IF EXISTS `preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preference` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `level` int(11) unsigned NOT NULL DEFAULT '100',
  `type` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `catagory` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `subcatagory` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catagory` (`catagory`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=146 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preference`
--

LOCK TABLES `preference` WRITE;
/*!40000 ALTER TABLE `preference` DISABLE KEYS */;
INSERT INTO `preference` VALUES (1,'download','1','Allow Downloads',100,'boolean','options','feature'),(4,'popular_threshold','10','Popular Threshold',25,'integer','interface','query'),(19,'transcode_bitrate','64','Transcode Bitrate',25,'string','streaming','transcoding'),(22,'site_title','Ampache :: For the love of Music','Website Title',100,'string','interface','custom'),(23,'lock_songs','0','Lock Songs',100,'boolean','system',NULL),(24,'force_http_play','0','Forces Http play regardless of port',100,'boolean','system',NULL),(41,'localplay_controller','0','Localplay Type',100,'special','options','localplay'),(29,'play_type','web_player','Type of Playback',25,'special','streaming',NULL),(31,'lang','es_ES','Language',100,'special','interface',NULL),(32,'playlist_type','m3u','Playlist Type',100,'special','playlist',NULL),(33,'theme_name','reborn','Theme',0,'special','interface','theme'),(51,'offset_limit','50','Offset Limit',5,'integer','interface','query'),(40,'localplay_level','0','Localplay Access',100,'special','options','localplay'),(44,'allow_stream_playback','1','Allow Streaming',100,'boolean','options','feature'),(45,'allow_democratic_playback','0','Allow Democratic Play',100,'boolean','options','feature'),(46,'allow_localplay_playback','0','Allow Localplay Play',100,'boolean','options','localplay'),(47,'stats_threshold','7','Statistics Day Threshold',25,'integer','interface','query'),(52,'rate_limit','8192','Rate Limit',100,'integer','streaming','transcoding'),(53,'playlist_method','default','Playlist Method',5,'string','playlist',NULL),(55,'transcode','default','Transcoding',25,'string','streaming','transcoding'),(69,'show_lyrics','0','Show Lyrics',0,'boolean','interface','player'),(70,'mpd_active','0','MPD Active Instance',25,'integer','internal','mpd'),(71,'httpq_active','0','HTTPQ Active Instance',25,'integer','internal','httpq'),(72,'shoutcast_active','0','Shoutcast Active Instance',25,'integer','internal','shoutcast'),(77,'lastfm_grant_link','','Last.FM Grant URL',25,'string','internal','lastfm'),(78,'lastfm_challenge','','Last.FM Submit Challenge',25,'string','internal','lastfm'),(102,'share','0','Allow Share',100,'boolean','options','feature'),(123,'ajax_load','1','Ajax page load',25,'boolean','interface',NULL),(82,'now_playing_per_user','1','Now playing filtered per user',50,'boolean','interface','home'),(83,'album_sort','0','Album Default Sort',25,'string','interface','library'),(84,'show_played_times','0','Show # played',25,'string','interface','library'),(85,'song_page_title','1','Show current song in Web player page title',25,'boolean','interface','player'),(86,'subsonic_backend','1','Use SubSonic backend',100,'boolean','system','backend'),(87,'plex_backend','0','Use Plex backend',100,'boolean','system','backend'),(88,'webplayer_flash','1','Authorize Flash Web Player(s)',25,'boolean','streaming','player'),(89,'webplayer_html5','1','Authorize HTML5 Web Player(s)',25,'boolean','streaming','player'),(90,'allow_personal_info_now','1','Personal information visibility - Now playing',25,'boolean','interface','privacy'),(91,'allow_personal_info_recent','1','Personal information visibility - Recently played',25,'boolean','interface','privacy'),(92,'allow_personal_info_time','1','Personal information visibility - Recently played - Allow to show streaming date/time',25,'boolean','interface','privacy'),(93,'allow_personal_info_agent','1','Personal information visibility - Recently played - Allow to show streaming agent',25,'boolean','interface','privacy'),(94,'ui_fixed','0','Fix header position on compatible themes',25,'boolean','interface','theme'),(95,'autoupdate','1','Check for Ampache updates automatically',25,'boolean','system','update'),(96,'autoupdate_lastcheck','','AutoUpdate last check time',25,'string','internal','update'),(97,'autoupdate_lastversion','','AutoUpdate last version from last check',25,'string','internal','update'),(98,'autoupdate_lastversion_new','','AutoUpdate last version from last check is newer',25,'boolean','internal','update'),(99,'webplayer_confirmclose','0','Confirmation when closing current playing window',25,'boolean','interface','player'),(100,'webplayer_pausetabs','1','Auto-pause betweens tabs',25,'boolean','interface','player'),(101,'stream_beautiful_url','0','Enable url rewriting',100,'boolean','streaming',NULL),(103,'share_expire','7','Share links default expiration days (0=never)',100,'integer','system','share'),(104,'slideshow_time','0','Artist slideshow inactivity time',25,'integer','interface','player'),(105,'broadcast_by_default','0','Broadcast web player by default',25,'boolean','streaming','player'),(106,'concerts_limit_future','0','Limit number of future events',25,'integer','interface','query'),(107,'concerts_limit_past','0','Limit number of past events',25,'integer','interface','query'),(108,'album_group','0','Album - Group multiple disks',25,'boolean','interface','library'),(109,'topmenu','0','Top menu',25,'boolean','interface','theme'),(110,'demo_clear_sessions','0','Clear democratic votes of expired user sessions',25,'boolean','playlist',NULL),(111,'show_donate','1','Show donate button in footer',25,'boolean','interface',NULL),(112,'upload_catalog','-1','Uploads catalog destination',75,'integer','system','upload'),(113,'allow_upload','0','Allow users to upload media',75,'boolean','system','upload'),(114,'upload_subdir','1','Upload: create a subdirectory per user (recommended)',75,'boolean','system','upload'),(115,'upload_user_artist','0','Upload: consider the user sender as the track\'s artist',75,'boolean','system','upload'),(116,'upload_script','','Upload: run the following script after upload (current directory = upload target directory)',75,'string','system','upload'),(117,'upload_allow_edit','1','Upload: allow users to edit uploaded songs',75,'boolean','system','upload'),(118,'daap_backend','0','Use DAAP backend',100,'boolean','system','backend'),(119,'daap_pass','','DAAP backend password',100,'string','system','backend'),(120,'upnp_backend','0','Use UPnP backend',100,'boolean','system','backend'),(121,'allow_video','1','Allow video features',75,'integer','options','feature'),(122,'album_release_type','1','Album - Group per release type',25,'boolean','interface','library'),(124,'direct_play_limit','0','Limit direct play to maximum media count',25,'integer','interface','player'),(125,'home_moment_albums','1','Show Albums of the moment at home page',25,'integer','interface','home'),(126,'home_moment_videos','1','Show Videos of the moment at home page',25,'integer','interface','home'),(127,'home_recently_played','1','Show Recently Played at home page',25,'integer','interface','home'),(128,'home_now_playing','1','Show Now Playing at home page',25,'integer','interface','home'),(129,'custom_logo','','Custom logo url',25,'string','interface','custom'),(130,'album_release_type_sort','album,ep,live,single','Album - Group per release type Sort',25,'string','interface','library'),(131,'browser_notify','1','WebPlayer browser notifications',25,'integer','interface','notification'),(132,'browser_notify_timeout','10','WebPlayer browser notifications timeout (seconds)',25,'integer','interface','notification'),(133,'geolocation','0','Allow geolocation',25,'integer','options','feature'),(134,'webplayer_aurora','1','Authorize JavaScript decoder (Aurora.js) in Web Player(s)',25,'boolean','streaming','player'),(135,'upload_allow_remove','1','Upload: allow users to remove uploaded songs',75,'boolean','system','upload'),(136,'custom_login_logo','','Custom login page logo url',75,'string','interface','custom'),(137,'custom_favicon','','Custom favicon url',75,'string','interface','custom'),(138,'custom_text_footer','','Custom text footer',75,'string','interface','custom'),(139,'webdav_backend','0','Use WebDAV backend',100,'boolean','system','backend'),(140,'notify_email','0','Receive notifications by email (shouts, private messages, ...)',25,'boolean','options',NULL),(141,'theme_color','dark','Theme color',0,'special','interface','theme'),(142,'disabled_custom_metadata_fields','','Disable custom metadata fields (ctrl / shift click to select multiple)',100,'string','system','metadata'),(143,'disabled_custom_metadata_fields_input','','Disable custom metadata fields. Insert them in a comma separated list. They will add to the fields selected above.',100,'string','system','metadata'),(144,'podcast_keep','10','Podcast: # latest episodes to keep',100,'integer','system','podcast'),(145,'podcast_new_download','1','Podcast: # episodes to download when new episodes are available',100,'integer','system','podcast');
/*!40000 ALTER TABLE `preference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `object_type` enum('artist','album','song','stream','video','playlist','tvshow','tvshow_season','podcast','podcast_episode') CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) unsigned NOT NULL DEFAULT '0',
  `rating` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_rating` (`user`,`object_type`,`object_id`),
  KEY `object_id` (`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recommendation`
--

DROP TABLE IF EXISTS `recommendation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommendation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `object_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `last_update` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recommendation`
--

LOCK TABLES `recommendation` WRITE;
/*!40000 ALTER TABLE `recommendation` DISABLE KEYS */;
/*!40000 ALTER TABLE `recommendation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recommendation_item`
--

DROP TABLE IF EXISTS `recommendation_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommendation_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `recommendation` int(11) unsigned NOT NULL,
  `recommendation_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `rel` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `mbid` varchar(1369) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recommendation_item`
--

LOCK TABLES `recommendation_item` WRITE;
/*!40000 ALTER TABLE `recommendation_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `recommendation_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search`
--

DROP TABLE IF EXISTS `search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `type` enum('private','public') CHARACTER SET utf8 DEFAULT NULL,
  `rules` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logic_operator` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `random` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `limit` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search`
--

LOCK TABLES `search` WRITE;
/*!40000 ALTER TABLE `search` DISABLE KEYS */;
/*!40000 ALTER TABLE `search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `id` varchar(256) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `username` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  `expire` int(11) unsigned NOT NULL DEFAULT '0',
  `value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ip` varbinary(255) DEFAULT NULL,
  `type` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  `agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `geo_latitude` decimal(10,6) DEFAULT NULL,
  `geo_longitude` decimal(10,6) DEFAULT NULL,
  `geo_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `expire` (`expire`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES ('db44716acc3b2428df2b2505edbe9dbb','',1526290359,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('4109a81a8d22faa34a64dba88b4f1816','pere',1526295989,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('5e86f992e08ab6b094ee79cce2ce25a8','pere',1526295916,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('74f7816e46c05f9c17e2e80b9fd6b051','',1526295922,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('763b66018d9cc51f09ea72f4bda298c9','admin',1526290418,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('27db3d871c421f1679fd212040cedc19','admin',1526290460,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('957c411a9fefc2f9ca9beb9f59122c84','admin',1526290581,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('f8a43a0d0d89531a9f83be63c39d3124','',1526290727,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('4a4e66b8c6383744005bf7587e8f4e4f','',1526290730,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('08e67ncahra5msbn4ajo48rsu2','admin',1526293756,'userdata|a:5:{s:7:\"success\";b:1;s:4:\"type\";s:5:\"mysql\";s:8:\"username\";s:5:\"admin\";s:3:\"uid\";s:1:\"1\";s:11:\"preferences\";a:92:{s:8:\"download\";s:1:\"1\";s:17:\"popular_threshold\";s:2:\"10\";s:17:\"transcode_bitrate\";s:2:\"32\";s:10:\"site_title\";s:32:\"Ampache :: For the love of Music\";s:10:\"lock_songs\";N;s:15:\"force_http_play\";N;s:20:\"localplay_controller\";s:3:\"mpd\";s:9:\"play_type\";s:10:\"web_player\";s:4:\"lang\";s:5:\"es_ES\";s:13:\"playlist_type\";s:3:\"m3u\";s:10:\"theme_name\";s:6:\"reborn\";s:12:\"offset_limit\";s:2:\"50\";s:15:\"localplay_level\";s:3:\"100\";s:21:\"allow_stream_playback\";s:1:\"1\";s:25:\"allow_democratic_playback\";s:1:\"1\";s:24:\"allow_localplay_playback\";s:1:\"1\";s:15:\"stats_threshold\";s:1:\"7\";s:10:\"rate_limit\";s:4:\"8192\";s:15:\"playlist_method\";s:7:\"default\";s:9:\"transcode\";s:7:\"default\";s:11:\"show_lyrics\";s:1:\"0\";s:10:\"mpd_active\";s:1:\"1\";s:12:\"httpq_active\";s:1:\"0\";s:16:\"shoutcast_active\";s:1:\"0\";s:17:\"lastfm_grant_link\";s:0:\"\";s:16:\"lastfm_challenge\";s:0:\"\";s:5:\"share\";s:1:\"0\";s:9:\"ajax_load\";s:1:\"1\";s:20:\"now_playing_per_user\";s:1:\"1\";s:10:\"album_sort\";s:1:\"0\";s:17:\"show_played_times\";s:1:\"0\";s:15:\"song_page_title\";s:1:\"1\";s:16:\"subsonic_backend\";s:1:\"1\";s:12:\"plex_backend\";N;s:15:\"webplayer_flash\";s:1:\"1\";s:15:\"webplayer_html5\";s:1:\"1\";s:23:\"allow_personal_info_now\";s:1:\"1\";s:26:\"allow_personal_info_recent\";s:1:\"1\";s:24:\"allow_personal_info_time\";s:1:\"1\";s:25:\"allow_personal_info_agent\";s:1:\"1\";s:8:\"ui_fixed\";s:1:\"0\";s:10:\"autoupdate\";s:1:\"1\";s:20:\"autoupdate_lastcheck\";s:10:\"1526286675\";s:22:\"autoupdate_lastversion\";s:5:\"3.8.6\";s:26:\"autoupdate_lastversion_new\";s:1:\"1\";s:22:\"webplayer_confirmclose\";s:1:\"0\";s:19:\"webplayer_pausetabs\";s:1:\"1\";s:20:\"stream_beautiful_url\";s:1:\"0\";s:12:\"share_expire\";s:1:\"7\";s:14:\"slideshow_time\";s:1:\"0\";s:20:\"broadcast_by_default\";s:1:\"0\";s:21:\"concerts_limit_future\";s:1:\"0\";s:19:\"concerts_limit_past\";s:1:\"0\";s:11:\"album_group\";s:1:\"0\";s:7:\"topmenu\";s:1:\"0\";s:19:\"demo_clear_sessions\";s:1:\"0\";s:11:\"show_donate\";s:1:\"1\";s:14:\"upload_catalog\";s:2:\"-1\";s:12:\"allow_upload\";N;s:13:\"upload_subdir\";s:1:\"1\";s:18:\"upload_user_artist\";N;s:13:\"upload_script\";N;s:17:\"upload_allow_edit\";s:1:\"1\";s:12:\"daap_backend\";N;s:9:\"daap_pass\";N;s:12:\"upnp_backend\";N;s:11:\"allow_video\";s:1:\"1\";s:18:\"album_release_type\";s:1:\"1\";s:17:\"direct_play_limit\";s:1:\"0\";s:18:\"home_moment_albums\";s:1:\"1\";s:18:\"home_moment_videos\";s:1:\"1\";s:20:\"home_recently_played\";s:1:\"1\";s:16:\"home_now_playing\";s:1:\"1\";s:11:\"custom_logo\";s:0:\"\";s:23:\"album_release_type_sort\";s:20:\"album,ep,live,single\";s:14:\"browser_notify\";s:1:\"1\";s:22:\"browser_notify_timeout\";s:2:\"10\";s:11:\"geolocation\";s:1:\"0\";s:16:\"webplayer_aurora\";s:1:\"1\";s:19:\"upload_allow_remove\";s:1:\"1\";s:17:\"custom_login_logo\";s:0:\"\";s:14:\"custom_favicon\";s:0:\"\";s:18:\"custom_text_footer\";s:0:\"\";s:14:\"webdav_backend\";N;s:12:\"notify_email\";s:1:\"0\";s:11:\"theme_color\";s:4:\"dark\";s:31:\"disabled_custom_metadata_fields\";N;s:37:\"disabled_custom_metadata_fields_input\";N;s:12:\"podcast_keep\";s:2:\"10\";s:20:\"podcast_new_download\";s:1:\"1\";s:10:\"theme_path\";s:14:\"/themes/reborn\";s:14:\"theme_css_base\";a:1:{i:0;a:2:{i:0;s:11:\"default.css\";i:1;s:6:\"screen\";}}}}art_enabled|b:1;login|b:0;state|a:1:{s:11:\"sidebar_tab\";s:4:\"home\";}catalog|i:0;browse_current_live_stream|a:1:{s:5:\"start\";i:0;}forms|a:4:{s:32:\"b941406c936792b57cfacf6d74a88d0f\";a:2:{s:4:\"name\";s:12:\"confirmation\";s:6:\"expire\";i:1526290492;}s:32:\"4ac0df3f740eb9857726b5189e24b614\";a:2:{s:4:\"name\";s:12:\"confirmation\";s:6:\"expire\";i:1526290546;}s:32:\"0f7f050ac249107c910d0605dfb55ab8\";a:2:{s:4:\"name\";s:12:\"confirmation\";s:6:\"expire\";i:1526290572;}s:32:\"632c1656df8905091937b1c6adc941bb\";a:2:{s:4:\"name\";s:9:\"add_radio\";s:6:\"expire\";i:1526290753;}}iframe|a:0:{}errors|a:1:{s:3:\"url\";s:41:\"URL invalida debe ser http:// or https://\";}browse_current_song|a:1:{s:5:\"start\";i:0;}browse_current_personal_video|a:1:{s:5:\"start\";i:0;}','�\n\n','mysql','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('644246271bf8878e8b5328fbdd7d2cdf','',1526293269,' ','��4','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('9938f57f7c46779f09a0ac75ef5b6b26','',1526290719,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('07233caace8ca93ec2b5591c0e96227d','',1526290664,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('b8f6583a9456957f47631521f0263298','',1526290666,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('e7c4a259d77f17dee3b611f2518d7f57','admin',1526290361,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('787d1678f39e9bda776d1af338ff0b2a','',1526290365,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('027f50fa56b950ff71d4707cc75ae3c9','',1526290368,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('8483bd26c40065454ca1e046f5df86d8','',1526290369,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('a208e356a7766bfa2fa1b9bbdd0220f0','',1526293875,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('cdb9bc1ba9f6fc6a5f3c0c198973f2d6','',1526290608,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('b2e38d87ddb9f109dd44b7549fac0d32','admin',1526290465,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('0d07d81581f333116a3181a923264c61','',1526293275,' ','��4','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('f4ccded1258804c462a6f4a509a6329c','pere',1526293278,' ','��4','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('0c476b067889bf8f68989f5028eb3874','',1526293283,' ','��4','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('378626c632748ef3203166d28fd341d9','pere',1526293284,' ','��4','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('937f65fde3ac45280bd0f9b9fea525c9','pere',1526293300,' ','��4','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('3f1a0342b3df0c29841c5667d38e273f','pere',1526293302,' ','��4','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('b9d7766ec13e14cc1029147ca1a6b693','pere',1526293325,' ','��4','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('2fdb99a57b1e5f47a9c23847960f7f86','',1526293350,' ','��4','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('ec64b06c058c65e28cd2119f23cf3ad0','',1526293877,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('0baec9b94415c2653b0669ef48e7c729','',1526294074,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('75650849f896b44dc1988fefe51ddc5e','',1526294092,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('87fc13eee0289b2cccf39a035337d96d','',1526294094,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('cba83eb31c823e638bb9b08f493bd586','admin',1526294096,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('cd83fe09a8e9a6743b4686a58b67fc27','admin',1526294104,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('fbe063e697cf7c89147dce9d86117a6d','',1526294130,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('8d598786e0a689191b53b120db49eff4','',1526294139,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('d3b5e08bb0da2fdd86d5b2988e8152e8','',1526294146,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('cb6710609c16a4f0ed12687fb7f35025','',1526294147,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('765c5420facae79e8180527b7d4bd664','admin',1526294149,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('22b19fb55c638abfb4c4489bff77f15f','',1526294165,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('7cdfe16c3555dd2e009bf244323925e8','',1526294172,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('96bf5af7903c8b4cf6cd9cd6ccf4b10a','',1526294174,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('9bee608e46c620fb04cf661d1b078100','admin',1526294176,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('cd78c584f0d2fdff15d38f853b94bfa1','admin',1526294183,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('a30e04afa1a160d61827939aabc93cfe','admin',1526294190,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('a7e4572eb59fbbdc8005a1f0e4819d89','admin',1526294196,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('69566ffe026d7312c0c53fb1e0bb25eb','admin',1526294200,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('e49f9c097887ea37f79bb2b89c4c7d72','',1526294353,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('87f4afb720d675ec18b73e018dba6fb6','',1526294814,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('c2d1eac08f9fac809eb7db239b06602f','admin',1526294824,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('5afd4cd0dc31c2a7c071056c1cc972a3','',1526295758,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('1b4837099efb2319a0dd41eeb8acac5f','',1526295761,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('e848ac1354ff152de108dc91672294a8','admin',1526295763,' ','�\n\n','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0',NULL,NULL,NULL),('1e77ir70gvc1url6utrpo7stp6','pere',1526292390,'userdata|a:7:{s:7:\"success\";b:1;s:4:\"type\";s:4:\"ldap\";s:8:\"username\";s:4:\"pere\";s:4:\"name\";s:4:\"pere\";s:5:\"email\";N;s:3:\"uid\";s:1:\"2\";s:11:\"preferences\";a:92:{s:8:\"download\";s:1:\"1\";s:17:\"popular_threshold\";s:2:\"10\";s:17:\"transcode_bitrate\";s:2:\"32\";s:10:\"site_title\";s:32:\"Ampache :: For the love of Music\";s:10:\"lock_songs\";N;s:15:\"force_http_play\";N;s:20:\"localplay_controller\";s:3:\"mpd\";s:9:\"play_type\";s:10:\"web_player\";s:4:\"lang\";s:5:\"es_ES\";s:13:\"playlist_type\";s:3:\"m3u\";s:10:\"theme_name\";s:6:\"reborn\";s:12:\"offset_limit\";s:2:\"50\";s:15:\"localplay_level\";s:3:\"100\";s:21:\"allow_stream_playback\";s:1:\"1\";s:25:\"allow_democratic_playback\";s:1:\"1\";s:24:\"allow_localplay_playback\";s:1:\"1\";s:15:\"stats_threshold\";s:1:\"7\";s:10:\"rate_limit\";s:4:\"8192\";s:15:\"playlist_method\";s:7:\"default\";s:9:\"transcode\";s:7:\"default\";s:11:\"show_lyrics\";s:1:\"0\";s:10:\"mpd_active\";s:1:\"0\";s:12:\"httpq_active\";s:1:\"0\";s:16:\"shoutcast_active\";s:1:\"0\";s:17:\"lastfm_grant_link\";s:0:\"\";s:16:\"lastfm_challenge\";s:0:\"\";s:5:\"share\";s:1:\"0\";s:9:\"ajax_load\";s:1:\"1\";s:20:\"now_playing_per_user\";s:1:\"1\";s:10:\"album_sort\";s:1:\"0\";s:17:\"show_played_times\";s:1:\"0\";s:15:\"song_page_title\";s:1:\"1\";s:16:\"subsonic_backend\";s:1:\"1\";s:12:\"plex_backend\";N;s:15:\"webplayer_flash\";s:1:\"1\";s:15:\"webplayer_html5\";s:1:\"1\";s:23:\"allow_personal_info_now\";s:1:\"1\";s:26:\"allow_personal_info_recent\";s:1:\"1\";s:24:\"allow_personal_info_time\";s:1:\"1\";s:25:\"allow_personal_info_agent\";s:1:\"1\";s:8:\"ui_fixed\";s:1:\"0\";s:10:\"autoupdate\";s:1:\"1\";s:20:\"autoupdate_lastcheck\";s:0:\"\";s:22:\"autoupdate_lastversion\";s:0:\"\";s:26:\"autoupdate_lastversion_new\";s:0:\"\";s:22:\"webplayer_confirmclose\";s:1:\"0\";s:19:\"webplayer_pausetabs\";s:1:\"1\";s:20:\"stream_beautiful_url\";s:1:\"0\";s:12:\"share_expire\";s:1:\"7\";s:14:\"slideshow_time\";s:1:\"0\";s:20:\"broadcast_by_default\";s:1:\"0\";s:21:\"concerts_limit_future\";s:1:\"0\";s:19:\"concerts_limit_past\";s:1:\"0\";s:11:\"album_group\";s:1:\"0\";s:7:\"topmenu\";s:1:\"0\";s:19:\"demo_clear_sessions\";s:1:\"0\";s:11:\"show_donate\";s:1:\"1\";s:14:\"upload_catalog\";s:2:\"-1\";s:12:\"allow_upload\";N;s:13:\"upload_subdir\";s:1:\"1\";s:18:\"upload_user_artist\";N;s:13:\"upload_script\";N;s:17:\"upload_allow_edit\";s:1:\"1\";s:12:\"daap_backend\";N;s:9:\"daap_pass\";N;s:12:\"upnp_backend\";N;s:11:\"allow_video\";s:1:\"1\";s:18:\"album_release_type\";s:1:\"1\";s:17:\"direct_play_limit\";s:1:\"0\";s:18:\"home_moment_albums\";s:1:\"1\";s:18:\"home_moment_videos\";s:1:\"1\";s:20:\"home_recently_played\";s:1:\"1\";s:16:\"home_now_playing\";s:1:\"1\";s:11:\"custom_logo\";s:0:\"\";s:23:\"album_release_type_sort\";s:20:\"album,ep,live,single\";s:14:\"browser_notify\";s:1:\"1\";s:22:\"browser_notify_timeout\";s:2:\"10\";s:11:\"geolocation\";s:1:\"0\";s:16:\"webplayer_aurora\";s:1:\"1\";s:19:\"upload_allow_remove\";s:1:\"1\";s:17:\"custom_login_logo\";s:0:\"\";s:14:\"custom_favicon\";s:0:\"\";s:18:\"custom_text_footer\";s:0:\"\";s:14:\"webdav_backend\";N;s:12:\"notify_email\";s:1:\"0\";s:11:\"theme_color\";s:4:\"dark\";s:31:\"disabled_custom_metadata_fields\";N;s:37:\"disabled_custom_metadata_fields_input\";N;s:12:\"podcast_keep\";s:2:\"10\";s:20:\"podcast_new_download\";s:1:\"1\";s:10:\"theme_path\";s:14:\"/themes/reborn\";s:14:\"theme_css_base\";a:1:{i:0;a:2:{i:0;s:11:\"default.css\";i:1;s:6:\"screen\";}}}}art_enabled|b:1;login|b:0;state|a:1:{s:11:\"sidebar_tab\";s:4:\"home\";}catalog|i:0;browse_current_song|a:1:{s:5:\"start\";i:0;}iframe|a:0:{}browse_current_personal_video|a:1:{s:5:\"start\";i:0;}browse_current_artist|a:1:{s:5:\"start\";i:0;}browse_current_playlist|a:1:{s:5:\"start\";i:0;}browse_current_album|a:1:{s:5:\"start\";i:0;}browse_current_live_stream|a:1:{s:5:\"start\";i:0;}browse_current_movie|a:1:{s:5:\"start\";i:0;}','��.','ldap','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('7df73a5eddc8f68dfb04d951ac886883','',1526295839,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('a3b34ed60048086170c29eb63a7c89fb','',1526295844,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('14eb0cbd2844a847db7d27c571459cc9','',1526295847,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('454effed277ecc3ca2b33fe8c6c44d1d','',1526295849,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('9758ab423d510bb18a30b83c1c970e1f','pere',1526295852,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('7a4a14c6a70fcd6f622a04c32602c141','pere',1526295864,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('1aeaf9a7dd2df154245efdac74362e49','',1526295896,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('41d9f37ecb7fcb73ecfb68c9dadc3be9','pere',1526295898,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('48c2005a2b866e6ca2d7cf43e1720e2f','',1526295923,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('dedc4e3d98f68bfd6701afbcacc96829','',1526295924,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('8270ccd292077d43b1449d8a85807e45','',1526295925,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('a415d9d77c8f0d1ad300e44bdb6e56d2','',1526295926,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('8d53e75b031e5a9e195c7986449f92a2','',1526295931,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('875009d43f12fb00441092c3ac11e697','',1526295934,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('0a16f4ba5136f94377beb6cfb1469178','',1526295936,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('d03e9c86f2dbd44037d9c641608f0f8c','pere',1526295938,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('d9584f22a3e8236a46b5d78f985fc814','',1526295939,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('64e9aeffa565c49ce51ddc14efd08b2f','',1526295945,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('6390c6887cdd6dfd752b03bd119f3026','',1526295947,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('a627831eecd3b76674f2690fb1790d6f','pere',1526295948,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL),('37c7870649fcc4ac4222839035c81dac','pere',1526295969,' ','��.','stream','Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',NULL,NULL,NULL);
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session_remember`
--

DROP TABLE IF EXISTS `session_remember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_remember` (
  `username` varchar(16) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `token` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `expire` int(11) DEFAULT NULL,
  PRIMARY KEY (`username`,`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session_remember`
--

LOCK TABLES `session_remember` WRITE;
/*!40000 ALTER TABLE `session_remember` DISABLE KEYS */;
/*!40000 ALTER TABLE `session_remember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session_stream`
--

DROP TABLE IF EXISTS `session_stream`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_stream` (
  `id` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `user` int(11) unsigned NOT NULL,
  `agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `expire` int(11) unsigned NOT NULL,
  `ip` varbinary(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session_stream`
--

LOCK TABLES `session_stream` WRITE;
/*!40000 ALTER TABLE `session_stream` DISABLE KEYS */;
/*!40000 ALTER TABLE `session_stream` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share`
--

DROP TABLE IF EXISTS `share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `share` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) unsigned NOT NULL,
  `object_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `allow_stream` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allow_download` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `expire_days` int(4) unsigned NOT NULL DEFAULT '0',
  `max_counter` int(4) unsigned NOT NULL DEFAULT '0',
  `secret` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `counter` int(4) unsigned NOT NULL DEFAULT '0',
  `creation_date` int(11) unsigned NOT NULL DEFAULT '0',
  `lastvisit_date` int(11) unsigned NOT NULL DEFAULT '0',
  `public_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share`
--

LOCK TABLES `share` WRITE;
/*!40000 ALTER TABLE `share` DISABLE KEYS */;
/*!40000 ALTER TABLE `share` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `song`
--

DROP TABLE IF EXISTS `song`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `catalog` int(11) unsigned NOT NULL DEFAULT '0',
  `album` int(11) unsigned NOT NULL DEFAULT '0',
  `year` mediumint(4) unsigned NOT NULL DEFAULT '0',
  `artist` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `bitrate` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rate` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `mode` enum('abr','vbr','cbr') CHARACTER SET utf8 DEFAULT NULL,
  `size` int(11) unsigned NOT NULL DEFAULT '0',
  `time` smallint(5) unsigned NOT NULL DEFAULT '0',
  `track` smallint(5) unsigned DEFAULT NULL,
  `mbid` varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  `played` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `update_time` int(11) unsigned DEFAULT '0',
  `addition_time` int(11) unsigned DEFAULT '0',
  `user_upload` int(11) DEFAULT NULL,
  `license` int(11) DEFAULT NULL,
  `composer` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `channels` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `album` (`album`),
  KEY `artist` (`artist`),
  KEY `file` (`file`(333)),
  KEY `update_time` (`update_time`),
  KEY `addition_time` (`addition_time`),
  KEY `catalog` (`catalog`),
  KEY `played` (`played`),
  KEY `enabled` (`enabled`),
  FULLTEXT KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `song`
--

LOCK TABLES `song` WRITE;
/*!40000 ALTER TABLE `song` DISABLE KEYS */;
INSERT INTO `song` VALUES (1,'/media/audio/true.mp3',2,1,0,1,'Ultra Violent',160000,44100,'cbr',6521070,326,0,NULL,0,1,0,1526281783,NULL,NULL,'',2),(2,'/media/audio/Beyoncé_-_Hold_Up_Video[ListenVid.com].mp3',2,1,0,2,'Beyoncé_-_Hold_Up_Video[ListenVid.com]',128010,44100,'vbr',5063711,316,0,NULL,1,1,0,1526281783,NULL,NULL,'',2),(3,'/media/audio/SampleAudio_0.4mb.mp3',2,1,0,2,'SampleAudio_0.4mb',128000,44100,'cbr',443926,27,0,NULL,0,1,0,1526281783,NULL,NULL,'',2),(4,'/media/audio/Beyoncé_-_Formation[ListenVid.com].mp3',2,1,0,2,'Beyoncé_-_Formation[ListenVid.com]',128011,44100,'vbr',4608136,287,0,NULL,1,1,0,1526281783,NULL,NULL,'',2),(5,'/media/audio/Beyoncé_-_Love_On_Top_Video_Edit[ListenVid.com].mp3',2,1,0,2,'Beyoncé_-_Love_On_Top_Video_Edit[ListenVid.com]',128016,44100,'vbr',3143189,196,0,NULL,0,1,0,1526281783,NULL,NULL,'',2);
/*!40000 ALTER TABLE `song` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `song_data`
--

DROP TABLE IF EXISTS `song_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_data` (
  `song_id` int(11) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `lyrics` text COLLATE utf8_unicode_ci,
  `label` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `catalog_number` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `language` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `waveform` mediumblob,
  `replaygain_track_gain` decimal(10,6) DEFAULT NULL,
  `replaygain_track_peak` decimal(10,6) DEFAULT NULL,
  `replaygain_album_gain` decimal(10,6) DEFAULT NULL,
  `replaygain_album_peak` decimal(10,6) DEFAULT NULL,
  UNIQUE KEY `song_id` (`song_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `song_data`
--

LOCK TABLES `song_data` WRITE;
/*!40000 ALTER TABLE `song_data` DISABLE KEYS */;
INSERT INTO `song_data` VALUES (1,'(Copyrighted music by \'HelRazor\' shared at www.SampleSwap.org) Creative Commons \'Attribution Non-commercial No Derivatives\' licensed. See: http://creativecommons.org/licenses/by-nc-nd/3.0/','','',NULL,'',NULL,0.000000,0.000000,0.000000,0.000000),(2,'','','',NULL,'',NULL,0.000000,0.000000,0.000000,0.000000),(3,'','','',NULL,'',NULL,0.000000,0.000000,0.000000,0.000000),(4,'','','',NULL,'',NULL,0.000000,0.000000,0.000000,0.000000),(5,'','','',NULL,'',NULL,0.000000,0.000000,0.000000,0.000000);
/*!40000 ALTER TABLE `song_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `song_preview`
--

DROP TABLE IF EXISTS `song_preview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_preview` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `artist` int(11) DEFAULT NULL,
  `artist_mbid` varchar(1369) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `album_mbid` varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  `mbid` varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  `disk` int(11) DEFAULT NULL,
  `track` int(11) DEFAULT NULL,
  `file` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `song_preview`
--

LOCK TABLES `song_preview` WRITE;
/*!40000 ALTER TABLE `song_preview` DISABLE KEYS */;
/*!40000 ALTER TABLE `song_preview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stream_playlist`
--

DROP TABLE IF EXISTS `stream_playlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stream_playlist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sid` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `info_url` text COLLATE utf8_unicode_ci,
  `image_url` text COLLATE utf8_unicode_ci,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `author` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `album` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `time` smallint(5) DEFAULT NULL,
  `codec` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `track_num` smallint(5) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sid` (`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stream_playlist`
--

LOCK TABLES `stream_playlist` WRITE;
/*!40000 ALTER TABLE `stream_playlist` DISABLE KEYS */;
INSERT INTO `stream_playlist` VALUES (63,'cd78c584f0d2fdff15d38f853b94bfa1','http://stream.radio.co/s601fe08ae/listen',NULL,NULL,'Radio - Rampa','Ampache',NULL,'live_stream',NULL,'mp3',0),(64,'a30e04afa1a160d61827939aabc93cfe','http://www.tnltvisira.com/',NULL,NULL,'Radio - isira','Ampache',NULL,'live_stream',NULL,'mp3',0),(62,'9bee608e46c620fb04cf661d1b078100','http://stream.radio.co/s601fe08ae/listen',NULL,NULL,'Radio - Rampa','Ampache',NULL,'live_stream',NULL,'mp3',0),(60,'cd83fe09a8e9a6743b4686a58b67fc27','http://www.tnltvisira.com/',NULL,NULL,'Radio - isira','Ampache',NULL,'live_stream',NULL,'mp3',0),(61,'765c5420facae79e8180527b7d4bd664','http://149.56.147.197:9641/cNeiva',NULL,NULL,'Radio - Colombia','Ampache',NULL,'live_stream',NULL,'mp3',0),(59,'cba83eb31c823e638bb9b08f493bd586','http://www.tnltvisira.com/',NULL,NULL,'Radio - isira','Ampache',NULL,'live_stream',NULL,'mp3',0),(58,'b9d7766ec13e14cc1029147ca1a6b693','http://192.168.2.59//play/index.php?ssid=b9d7766ec13e14cc1029147ca1a6b693&type=video&oid=1&uid=2&name=SampleVideo-1280x720-1mb.mp4',NULL,NULL,'Video - SampleVideo_1280x720_1mb',NULL,NULL,'video',5,NULL,0),(57,'3f1a0342b3df0c29841c5667d38e273f','http://192.168.2.59//play/index.php?ssid=3f1a0342b3df0c29841c5667d38e273f&type=video&oid=1&uid=2&name=SampleVideo-1280x720-1mb.mp4',NULL,NULL,'Video - SampleVideo_1280x720_1mb',NULL,NULL,'video',5,NULL,0),(56,'937f65fde3ac45280bd0f9b9fea525c9','http://192.168.2.59//play/index.php?ssid=937f65fde3ac45280bd0f9b9fea525c9&type=video&oid=2&uid=2&name=SampleVideo-720x480-1mb.mkv',NULL,NULL,'Video - SampleVideo_720x480_1mb',NULL,NULL,'video',5,NULL,0),(55,'378626c632748ef3203166d28fd341d9','http://192.168.2.59//play/index.php?ssid=378626c632748ef3203166d28fd341d9&type=video&oid=1&uid=2&name=SampleVideo-1280x720-1mb.mp4',NULL,NULL,'Video - SampleVideo_1280x720_1mb',NULL,NULL,'video',5,NULL,0),(54,'f4ccded1258804c462a6f4a509a6329c','http://192.168.2.59//play/index.php?ssid=f4ccded1258804c462a6f4a509a6329c&type=song&oid=4&uid=2&name=Desconocido%20-Huerfano-%20-%20Beyonc-Formation-ListenVid.com-.mp3','<a href=\"http://192.168.2.59//song.php?action=show_song&amp;song_id=4\" title=\"Desconocido (Huerfano) - Beyonc&eacute;_-_Formation[ListenVid.com]\"> Beyonc&eacute;_-_Formation[ListenVid.com]</a>','http://192.168.2.59//image.php?object_id=1&object_type=album&auth=f4ccded1258804c462a6f4a509a6329c&thumb=3','Beyoncé_-_Formation[ListenVid.com]','Desconocido (Huerfano)','Desconocido (Huerfano)','song',287,NULL,0),(53,'957c411a9fefc2f9ca9beb9f59122c84','http://localhost//play/index.php?ssid=957c411a9fefc2f9ca9beb9f59122c84&type=video&oid=1&uid=1&name=SampleVideo-1280x720-1mb.mp4',NULL,NULL,'Video - SampleVideo_1280x720_1mb',NULL,NULL,'video',5,NULL,0),(52,'b2e38d87ddb9f109dd44b7549fac0d32','http://localhost//play/index.php?ssid=b2e38d87ddb9f109dd44b7549fac0d32&type=video&oid=1&uid=1&name=SampleVideo-1280x720-1mb.mp4',NULL,NULL,'Video - SampleVideo_1280x720_1mb',NULL,NULL,'video',5,NULL,0),(51,'27db3d871c421f1679fd212040cedc19','http://localhost//play/index.php?ssid=27db3d871c421f1679fd212040cedc19&type=video&oid=1&uid=1&name=SampleVideo-1280x720-1mb.mp4',NULL,NULL,'Video - SampleVideo_1280x720_1mb',NULL,NULL,'video',5,NULL,0),(50,'763b66018d9cc51f09ea72f4bda298c9','http://localhost//play/index.php?ssid=763b66018d9cc51f09ea72f4bda298c9&type=video&oid=1&uid=1&name=SampleVideo-1280x720-1mb.mp4',NULL,NULL,'Video - SampleVideo_1280x720_1mb',NULL,NULL,'video',5,NULL,0),(49,'e7c4a259d77f17dee3b611f2518d7f57','http://localhost//play/index.php?ssid=e7c4a259d77f17dee3b611f2518d7f57&type=song&oid=4&uid=1&name=Desconocido%20-Huerfano-%20-%20Beyonc-Formation-ListenVid.com-.mp3','<a href=\"http://localhost//song.php?action=show_song&amp;song_id=4\" title=\"Desconocido (Huerfano) - Beyonc&eacute;_-_Formation[ListenVid.com]\"> Beyonc&eacute;_-_Formation[ListenVid.com]</a>','http://localhost//image.php?object_id=1&object_type=album&auth=e7c4a259d77f17dee3b611f2518d7f57&thumb=3','Beyoncé_-_Formation[ListenVid.com]','Desconocido (Huerfano)','Desconocido (Huerfano)','song',287,NULL,0),(65,'a7e4572eb59fbbdc8005a1f0e4819d89','http://www.tnltvisira.com/',NULL,NULL,'Radio - isira','Ampache',NULL,'live_stream',NULL,'mp3',0),(66,'69566ffe026d7312c0c53fb1e0bb25eb','http://149.56.147.197:9641/cNeiva',NULL,NULL,'Radio - Colombia','Ampache',NULL,'live_stream',NULL,'mp3',0),(67,'c2d1eac08f9fac809eb7db239b06602f','http://stream.radio.co/s601fe08ae/listen',NULL,NULL,'Radio - Rampa','Ampache',NULL,'live_stream',NULL,'mp3',0),(68,'e848ac1354ff152de108dc91672294a8','http://localhost//play/index.php?ssid=e848ac1354ff152de108dc91672294a8&type=video&oid=1&uid=1&name=SampleVideo-1280x720-1mb.mp4',NULL,NULL,'Video - SampleVideo_1280x720_1mb',NULL,NULL,'video',5,NULL,0),(69,'9758ab423d510bb18a30b83c1c970e1f','http://192.168.2.59//play/index.php?ssid=9758ab423d510bb18a30b83c1c970e1f&type=song&oid=2&uid=2&name=Desconocido%20-Huerfano-%20-%20Beyonc-Hold-Up-Video-ListenVid.com-.mp3','<a href=\"http://192.168.2.59//song.php?action=show_song&amp;song_id=2\" title=\"Desconocido (Huerfano) - Beyonc&eacute;_-_Hold_Up_Video[ListenVid.com]\"> Beyonc&eacute;_-_Hold_Up_Video[ListenVid.com]</a>','http://192.168.2.59//image.php?object_id=1&object_type=album&auth=9758ab423d510bb18a30b83c1c970e1f&thumb=3','Beyoncé_-_Hold_Up_Video[ListenVid.com]','Desconocido (Huerfano)','Desconocido (Huerfano)','song',316,NULL,0),(70,'7a4a14c6a70fcd6f622a04c32602c141','http://192.168.2.59//play/index.php?ssid=7a4a14c6a70fcd6f622a04c32602c141&type=song&oid=2&uid=2&name=Desconocido%20-Huerfano-%20-%20Beyonc-Hold-Up-Video-ListenVid.com-.mp3','<a href=\"http://192.168.2.59//song.php?action=show_song&amp;song_id=2\" title=\"Desconocido (Huerfano) - Beyonc&eacute;_-_Hold_Up_Video[ListenVid.com]\"> Beyonc&eacute;_-_Hold_Up_Video[ListenVid.com]</a>','http://192.168.2.59//image.php?object_id=1&object_type=album&auth=7a4a14c6a70fcd6f622a04c32602c141&thumb=3','Beyoncé_-_Hold_Up_Video[ListenVid.com]','Desconocido (Huerfano)','Desconocido (Huerfano)','song',316,NULL,0),(71,'41d9f37ecb7fcb73ecfb68c9dadc3be9','http://192.168.2.59//play/index.php?ssid=41d9f37ecb7fcb73ecfb68c9dadc3be9&type=video&oid=1&uid=2&name=SampleVideo-1280x720-1mb.mp4',NULL,NULL,'Video - SampleVideo_1280x720_1mb',NULL,NULL,'video',5,NULL,0),(72,'5e86f992e08ab6b094ee79cce2ce25a8','http://192.168.2.59//play/index.php?ssid=5e86f992e08ab6b094ee79cce2ce25a8&type=video&oid=2&uid=2&name=SampleVideo-720x480-1mb.mkv',NULL,NULL,'Video - SampleVideo_720x480_1mb',NULL,NULL,'video',5,NULL,0),(73,'d03e9c86f2dbd44037d9c641608f0f8c','http://192.168.2.59//play/index.php?ssid=d03e9c86f2dbd44037d9c641608f0f8c&type=video&oid=1&uid=2&name=SampleVideo-1280x720-1mb.mp4',NULL,NULL,'Video - SampleVideo_1280x720_1mb',NULL,NULL,'video',5,NULL,0),(74,'a627831eecd3b76674f2690fb1790d6f','http://149.56.147.197:9641/cNeiva',NULL,NULL,'Radio - Colombia','Ampache',NULL,'live_stream',NULL,'mp3',0),(75,'37c7870649fcc4ac4222839035c81dac','http://stream.radio.co/s601fe08ae/listen',NULL,NULL,'Radio - Rampa','Ampache',NULL,'live_stream',NULL,'mp3',0),(76,'4109a81a8d22faa34a64dba88b4f1816','http://www.tnltvisira.com/',NULL,NULL,'Radio - isira','Ampache',NULL,'live_stream',NULL,'mp3',0);
/*!40000 ALTER TABLE `stream_playlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `name` (`name`),
  KEY `map_id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_map`
--

DROP TABLE IF EXISTS `tag_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_map` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) unsigned NOT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `object_type` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_tag_map` (`object_id`,`object_type`,`user`,`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_map`
--

LOCK TABLES `tag_map` WRITE;
/*!40000 ALTER TABLE `tag_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_merge`
--

DROP TABLE IF EXISTS `tag_merge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_merge` (
  `tag_id` int(11) NOT NULL,
  `merged_to` int(11) NOT NULL,
  PRIMARY KEY (`tag_id`,`merged_to`),
  KEY `merged_to` (`merged_to`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_merge`
--

LOCK TABLES `tag_merge` WRITE;
/*!40000 ALTER TABLE `tag_merge` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag_merge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_browse`
--

DROP TABLE IF EXISTS `tmp_browse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_browse` (
  `id` int(13) NOT NULL AUTO_INCREMENT,
  `sid` varchar(128) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `object_data` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`sid`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_browse`
--

LOCK TABLES `tmp_browse` WRITE;
/*!40000 ALTER TABLE `tmp_browse` DISABLE KEYS */;
INSERT INTO `tmp_browse` VALUES (4,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `artist` \",\"filter\":{\"catalog\":0},\"select\":[\"`artist`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"artist\",\"update_session\":true,\"sort\":{\"name\":\"ASC\"},\"total\":2}',NULL),(3,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `personal_video` \",\"filter\":[],\"select\":[\"`personal_video`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"personal_video\",\"update_session\":true,\"total\":2}',NULL),(2,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `song` \",\"filter\":{\"catalog\":0},\"select\":[\"`song`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"song\",\"update_session\":true,\"sort\":{\"title\":\"ASC\"},\"total\":5}',NULL),(7,'08e67ncahra5msbn4ajo48rsu2','{\"base\":\"SELECT %%SELECT%% FROM `personal_video` \",\"filter\":[],\"select\":[\"`personal_video`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"personal_video\",\"update_session\":true,\"total\":2}',NULL),(1,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `song` \",\"filter\":{\"catalog\":0},\"select\":[\"`song`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"song\",\"update_session\":true,\"sort\":{\"title\":\"ASC\"},\"total\":5}',NULL),(6,'08e67ncahra5msbn4ajo48rsu2','{\"base\":\"SELECT %%SELECT%% FROM `song` \",\"filter\":{\"catalog\":0},\"select\":[\"`song`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"song\",\"update_session\":true,\"sort\":{\"title\":\"ASC\"},\"total\":5}',NULL),(4,'08e67ncahra5msbn4ajo48rsu2','{\"base\":\"SELECT %%SELECT%% FROM `live_stream` \",\"filter\":[],\"select\":[\"`live_stream`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"live_stream\",\"update_session\":true,\"sort\":{\"name\":\"ASC\"},\"total\":3}',NULL),(5,'08e67ncahra5msbn4ajo48rsu2','{\"base\":\"SELECT %%SELECT%% FROM `live_stream` \",\"filter\":[],\"select\":[\"`live_stream`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"live_stream\",\"update_session\":true,\"sort\":{\"name\":\"ASC\"},\"total\":3}',NULL),(3,'08e67ncahra5msbn4ajo48rsu2','{\"base\":\"SELECT %%SELECT%% FROM `live_stream` \",\"filter\":[],\"select\":[\"`live_stream`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"live_stream\",\"update_session\":true,\"sort\":{\"name\":\"ASC\"},\"total\":2}',NULL),(1,'08e67ncahra5msbn4ajo48rsu2','{\"base\":\"SELECT %%SELECT%% FROM `live_stream` \",\"filter\":[],\"select\":[\"`live_stream`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"live_stream\",\"update_session\":true,\"sort\":{\"name\":\"ASC\"},\"total\":0}',NULL),(2,'08e67ncahra5msbn4ajo48rsu2','{\"base\":\"SELECT %%SELECT%% FROM `live_stream` \",\"filter\":[],\"select\":[\"`live_stream`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"live_stream\",\"update_session\":true,\"sort\":{\"name\":\"ASC\"},\"total\":1}',NULL),(5,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `playlist` \",\"filter\":{\"playlist_type\":\"1\"},\"select\":[\"`playlist`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"playlist\",\"update_session\":true,\"total\":0}',NULL),(6,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `tag` \",\"filter\":[],\"select\":[\"`tag`.`id`\"],\"static\":false,\"simple\":false,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"tag\",\"join\":{\"1\":{\"tag_map\":\"LEFT JOIN tag_map ON `tag_map`.`tag_id`=`tag`.`id`\"}},\"update_session\":true,\"total\":0}','[]'),(7,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `artist` \",\"filter\":[],\"select\":[\"`artist`.`id`\"],\"static\":false,\"simple\":false,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"artist\"}',NULL),(8,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `album` \",\"filter\":{\"catalog\":0},\"select\":[\"`album`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"album\",\"update_session\":true,\"sort\":{\"name\":\"ASC\"},\"total\":1}',NULL),(9,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `song` \",\"filter\":{\"catalog\":0},\"select\":[\"`song`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"song\",\"update_session\":true,\"sort\":{\"title\":\"ASC\"},\"total\":5}',NULL),(10,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `tag` \",\"filter\":[],\"select\":[\"`tag`.`id`\"],\"static\":false,\"simple\":false,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"tag\",\"join\":{\"1\":{\"tag_map\":\"LEFT JOIN tag_map ON `tag_map`.`tag_id`=`tag`.`id`\"}},\"update_session\":true,\"total\":0}','[]'),(11,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `video` \",\"filter\":[],\"select\":[\"`video`.`id`\"],\"static\":false,\"simple\":false,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"video\"}',NULL),(12,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `personal_video` \",\"filter\":[],\"select\":[\"`personal_video`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"personal_video\",\"update_session\":true,\"total\":2}',NULL),(13,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `live_stream` \",\"filter\":[],\"select\":[\"`live_stream`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"live_stream\",\"update_session\":true,\"sort\":{\"name\":\"ASC\"},\"total\":3}',NULL),(14,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `movie` \",\"filter\":[],\"select\":[\"`movie`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"movie\",\"update_session\":true,\"total\":0}',NULL),(15,'1e77ir70gvc1url6utrpo7stp6','{\"base\":\"SELECT %%SELECT%% FROM `live_stream` \",\"filter\":[],\"select\":[\"`live_stream`.`id`\"],\"static\":false,\"simple\":true,\"start\":0,\"offset\":50,\"use_pages\":true,\"use_alpha\":false,\"grid_view\":true,\"type\":\"live_stream\",\"update_session\":true,\"sort\":{\"name\":\"ASC\"},\"total\":3}',NULL);
/*!40000 ALTER TABLE `tmp_browse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_playlist`
--

DROP TABLE IF EXISTS `tmp_playlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_playlist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `object_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session` (`session`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_playlist`
--

LOCK TABLES `tmp_playlist` WRITE;
/*!40000 ALTER TABLE `tmp_playlist` DISABLE KEYS */;
INSERT INTO `tmp_playlist` VALUES (7,'1e77ir70gvc1url6utrpo7stp6','user','song'),(6,'08e67ncahra5msbn4ajo48rsu2','user','song');
/*!40000 ALTER TABLE `tmp_playlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_playlist_data`
--

DROP TABLE IF EXISTS `tmp_playlist_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_playlist_data` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tmp_playlist` int(11) unsigned NOT NULL,
  `object_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `track` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tmp_playlist` (`tmp_playlist`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_playlist_data`
--

LOCK TABLES `tmp_playlist_data` WRITE;
/*!40000 ALTER TABLE `tmp_playlist_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmp_playlist_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tvshow`
--

DROP TABLE IF EXISTS `tvshow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tvshow` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `summary` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `year` int(11) unsigned DEFAULT NULL,
  `prefix` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tvshow`
--

LOCK TABLES `tvshow` WRITE;
/*!40000 ALTER TABLE `tvshow` DISABLE KEYS */;
/*!40000 ALTER TABLE `tvshow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tvshow_episode`
--

DROP TABLE IF EXISTS `tvshow_episode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tvshow_episode` (
  `id` int(11) unsigned NOT NULL,
  `original_name` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `season` int(11) unsigned NOT NULL,
  `episode_number` int(11) unsigned NOT NULL,
  `summary` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tvshow_episode`
--

LOCK TABLES `tvshow_episode` WRITE;
/*!40000 ALTER TABLE `tvshow_episode` DISABLE KEYS */;
/*!40000 ALTER TABLE `tvshow_episode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tvshow_season`
--

DROP TABLE IF EXISTS `tvshow_season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tvshow_season` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `season_number` int(11) unsigned NOT NULL,
  `tvshow` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tvshow_season`
--

LOCK TABLES `tvshow_season` WRITE;
/*!40000 ALTER TABLE `tvshow_season` DISABLE KEYS */;
/*!40000 ALTER TABLE `tvshow_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `update_info`
--

DROP TABLE IF EXISTS `update_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `update_info` (
  `key` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `update_info`
--

LOCK TABLES `update_info` WRITE;
/*!40000 ALTER TABLE `update_info` DISABLE KEYS */;
INSERT INTO `update_info` VALUES ('db_version','380005'),('Plugin_Last.FM','000005');
/*!40000 ALTER TABLE `update_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `fullname` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `apikey` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `access` tinyint(4) unsigned NOT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `last_seen` int(11) unsigned NOT NULL DEFAULT '0',
  `create_date` int(11) unsigned DEFAULT NULL,
  `validation` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `state` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `fullname_public` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','Administrator','',NULL,NULL,'24422502db043af6dc9fe7b30389de9e23acef19f5fff8724930c24913e85955',100,0,1526290156,1525852570,NULL,NULL,NULL,0),(2,'pere','pere',NULL,NULL,NULL,'0ab92e5e0ba5cdbb91ea01e63693919787d0b7d7eccfbd4bc9b67fe3a75e522e',25,0,1526288790,1526283519,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_activity`
--

DROP TABLE IF EXISTS `user_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) unsigned NOT NULL,
  `action` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `object_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `activity_date` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_activity`
--

LOCK TABLES `user_activity` WRITE;
/*!40000 ALTER TABLE `user_activity` DISABLE KEYS */;
INSERT INTO `user_activity` VALUES (1,1,'play',1,'video',1525852650),(2,1,'play',1,'video',1525852690),(3,1,'play',1,'video',1525852822),(4,1,'play',1,'video',1525852834),(5,1,'play',1,'video',1525852867),(6,1,'play',1,'video',1525852907),(7,1,'play',1,'video',1525852963),(8,1,'play',1,'video',1525858191),(9,1,'play',1,'video',1525858202),(10,1,'play',1,'video',1525858213),(11,1,'play',1,'video',1525858225),(12,1,'play',1,'video',1525858236),(13,1,'play',1,'video',1525858247),(14,1,'play',1,'video',1525858258),(15,1,'play',1,'video',1525858270),(16,1,'play',1,'video',1526283218),(17,1,'play',1,'video',1526283244),(18,1,'play',1,'video',1526283260),(19,1,'play',1,'video',1526283381),(20,2,'play',4,'song',1526286078),(21,2,'play',1,'video',1526286125),(22,1,'play',1,'video',1526288563),(23,2,'play',2,'song',1526288663);
/*!40000 ALTER TABLE `user_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_catalog`
--

DROP TABLE IF EXISTS `user_catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_catalog` (
  `user` int(11) unsigned NOT NULL,
  `catalog` int(11) unsigned NOT NULL,
  `level` smallint(4) unsigned NOT NULL DEFAULT '5',
  KEY `user` (`user`),
  KEY `catalog` (`catalog`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_catalog`
--

LOCK TABLES `user_catalog` WRITE;
/*!40000 ALTER TABLE `user_catalog` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_flag`
--

DROP TABLE IF EXISTS `user_flag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_flag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `object_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `date` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_userflag` (`user`,`object_type`,`object_id`),
  KEY `object_id` (`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_flag`
--

LOCK TABLES `user_flag` WRITE;
/*!40000 ALTER TABLE `user_flag` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_flag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_follower`
--

DROP TABLE IF EXISTS `user_follower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_follower` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) unsigned NOT NULL,
  `follow_user` int(11) unsigned NOT NULL,
  `follow_date` int(11) unsigned DEFAULT NULL,
  `creation_date` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_follower`
--

LOCK TABLES `user_follower` WRITE;
/*!40000 ALTER TABLE `user_follower` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_follower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_preference`
--

DROP TABLE IF EXISTS `user_preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_preference` (
  `user` int(11) NOT NULL,
  `preference` int(11) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `user` (`user`),
  KEY `preference` (`preference`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_preference`
--

LOCK TABLES `user_preference` WRITE;
/*!40000 ALTER TABLE `user_preference` DISABLE KEYS */;
INSERT INTO `user_preference` VALUES (-1,1,'1'),(-1,4,'10'),(-1,19,'32'),(-1,22,'Ampache :: For the love of Music'),(-1,23,'0'),(-1,24,'0'),(-1,25,'80'),(-1,41,'mpd'),(-1,29,'web_player'),(-1,31,'es_ES'),(-1,32,'m3u'),(-1,33,'reborn'),(-1,34,'27'),(-1,35,'27'),(-1,36,'27'),(-1,51,'50'),(-1,40,'100'),(-1,44,'1'),(-1,45,'1'),(-1,46,'1'),(-1,47,'7'),(-1,49,'1'),(-1,52,'8192'),(-1,53,'default'),(-1,55,'default'),(-1,57,''),(-1,69,'0'),(-1,70,'0'),(-1,71,'0'),(-1,72,'0'),(-1,77,''),(-1,78,''),(-1,114,'1'),(-1,113,'0'),(-1,112,'-1'),(-1,111,'1'),(-1,110,'0'),(-1,109,'0'),(-1,108,'0'),(-1,107,'0'),(-1,106,'0'),(-1,105,'0'),(-1,104,'0'),(-1,103,'7'),(-1,102,'0'),(-1,101,'0'),(-1,100,'1'),(-1,99,'0'),(-1,95,'1'),(-1,94,'0'),(-1,93,'1'),(-1,92,'1'),(-1,91,'1'),(-1,90,'1'),(-1,89,'1'),(-1,88,'1'),(-1,87,'0'),(-1,86,'1'),(-1,85,'1'),(-1,84,'0'),(-1,83,'0'),(-1,79,'50'),(-1,80,'50'),(-1,82,'1'),(-1,81,'1'),(-1,115,'0'),(-1,116,''),(-1,117,'1'),(-1,118,'0'),(-1,119,''),(-1,120,'0'),(-1,121,'1'),(-1,122,'1'),(-1,123,'1'),(-1,124,'0'),(-1,125,'1'),(-1,126,'1'),(-1,127,'1'),(-1,128,'1'),(-1,129,''),(-1,130,'album,ep,live,single'),(-1,131,'1'),(-1,132,'10'),(-1,133,'0'),(-1,134,'1'),(-1,135,'1'),(-1,136,''),(-1,137,''),(-1,138,''),(-1,139,'0'),(-1,140,'0'),(-1,141,'dark'),(-1,142,''),(-1,143,''),(-1,96,''),(-1,97,''),(-1,98,''),(-1,144,'10'),(-1,145,'1'),(1,1,'1'),(1,4,'10'),(1,19,'32'),(1,22,'Ampache :: For the love of Music'),(1,41,'mpd'),(1,29,'web_player'),(1,31,'es_ES'),(1,32,'m3u'),(1,33,'reborn'),(1,51,'50'),(1,40,'100'),(1,44,'1'),(1,45,'1'),(1,46,'1'),(1,47,'7'),(1,52,'8192'),(1,53,'default'),(1,55,'default'),(1,69,'0'),(1,70,'1'),(1,71,'0'),(1,72,'0'),(1,77,''),(1,78,''),(1,102,'0'),(1,123,'1'),(1,82,'1'),(1,83,'0'),(1,84,'0'),(1,85,'1'),(1,88,'1'),(1,89,'1'),(1,90,'1'),(1,91,'1'),(1,92,'1'),(1,93,'1'),(1,94,'0'),(1,96,'1526286675'),(1,97,'3.8.6'),(1,98,'1'),(1,99,'0'),(1,100,'1'),(1,101,'0'),(1,104,'0'),(1,105,'0'),(1,106,'0'),(1,107,'0'),(1,108,'0'),(1,109,'0'),(1,110,'0'),(1,111,'1'),(1,121,'1'),(1,122,'1'),(1,124,'0'),(1,125,'1'),(1,126,'1'),(1,127,'1'),(1,128,'1'),(1,129,''),(1,130,'album,ep,live,single'),(1,131,'1'),(1,132,'10'),(1,133,'0'),(1,134,'1'),(1,136,''),(1,137,''),(1,138,''),(1,140,'0'),(1,141,'dark'),(2,1,'1'),(2,4,'10'),(2,19,'32'),(2,22,'Ampache :: For the love of Music'),(2,41,'mpd'),(2,29,'web_player'),(2,31,'es_ES'),(2,32,'m3u'),(2,33,'reborn'),(2,51,'50'),(2,40,'100'),(2,44,'1'),(2,45,'1'),(2,46,'1'),(2,47,'7'),(2,52,'8192'),(2,53,'default'),(2,55,'default'),(2,69,'0'),(2,70,'0'),(2,71,'0'),(2,72,'0'),(2,77,''),(2,78,''),(2,102,'0'),(2,123,'1'),(2,82,'1'),(2,83,'0'),(2,84,'0'),(2,85,'1'),(2,88,'1'),(2,89,'1'),(2,90,'1'),(2,91,'1'),(2,92,'1'),(2,93,'1'),(2,94,'0'),(2,96,''),(2,97,''),(2,98,''),(2,99,'0'),(2,100,'1'),(2,101,'0'),(2,104,'0'),(2,105,'0'),(2,106,'0'),(2,107,'0'),(2,108,'0'),(2,109,'0'),(2,110,'0'),(2,111,'1'),(2,121,'1'),(2,122,'1'),(2,124,'0'),(2,125,'1'),(2,126,'1'),(2,127,'1'),(2,128,'1'),(2,129,''),(2,130,'album,ep,live,single'),(2,131,'1'),(2,132,'10'),(2,133,'0'),(2,134,'1'),(2,136,''),(2,137,''),(2,138,''),(2,140,'0'),(2,141,'dark');
/*!40000 ALTER TABLE `user_preference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_pvmsg`
--

DROP TABLE IF EXISTS `user_pvmsg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_pvmsg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `message` text CHARACTER SET utf8,
  `from_user` int(11) unsigned NOT NULL,
  `to_user` int(11) unsigned NOT NULL,
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `creation_date` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_pvmsg`
--

LOCK TABLES `user_pvmsg` WRITE;
/*!40000 ALTER TABLE `user_pvmsg` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_pvmsg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_shout`
--

DROP TABLE IF EXISTS `user_shout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_shout` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `date` int(11) unsigned NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `object_id` int(11) unsigned NOT NULL,
  `object_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `data` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sticky` (`sticky`),
  KEY `date` (`date`),
  KEY `user` (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_shout`
--

LOCK TABLES `user_shout` WRITE;
/*!40000 ALTER TABLE `user_shout` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_shout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_vote`
--

DROP TABLE IF EXISTS `user_vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_vote` (
  `user` int(11) unsigned NOT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `date` int(11) unsigned NOT NULL,
  `sid` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  KEY `user` (`user`),
  KEY `object_id` (`object_id`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_vote`
--

LOCK TABLES `user_vote` WRITE;
/*!40000 ALTER TABLE `user_vote` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_vote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `catalog` int(11) unsigned NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_codec` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `audio_codec` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `resolution_x` mediumint(8) unsigned NOT NULL,
  `resolution_y` mediumint(8) unsigned NOT NULL,
  `time` int(11) unsigned NOT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `mime` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `addition_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `played` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `release_date` int(11) DEFAULT NULL,
  `channels` mediumint(9) DEFAULT NULL,
  `bitrate` mediumint(8) DEFAULT NULL,
  `video_bitrate` mediumint(8) DEFAULT NULL,
  `display_x` mediumint(8) DEFAULT NULL,
  `display_y` mediumint(8) DEFAULT NULL,
  `frame_rate` float DEFAULT NULL,
  `mode` enum('abr','vbr','cbr') CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `file` (`file`(333)),
  KEY `enabled` (`enabled`),
  KEY `title` (`title`),
  KEY `addition_time` (`addition_time`),
  KEY `update_time` (`update_time`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
INSERT INTO `video` VALUES (1,'/media/video/personales/SampleVideo_1280x720_1mb.mp4',1,'SampleVideo_1280x720_1mb','quicktime','mp4',1280,720,5,1055736,'video/quicktime',1525852638,NULL,1,1,0,2,0,0,0,0,25,NULL),(2,'/media/video/personales/SampleVideo_720x480_1mb.mkv',1,'SampleVideo_720x480_1mb','mpeg4','aac',720,480,5,1050238,'video/x-matroska',1525852638,NULL,1,0,0,6,0,0,720,480,25,NULL);
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wanted`
--

DROP TABLE IF EXISTS `wanted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wanted` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `artist` int(11) DEFAULT NULL,
  `artist_mbid` varchar(1369) CHARACTER SET utf8 DEFAULT NULL,
  `mbid` varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `date` int(11) unsigned NOT NULL DEFAULT '0',
  `accepted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_wanted` (`user`,`artist`,`mbid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wanted`
--

LOCK TABLES `wanted` WRITE;
/*!40000 ALTER TABLE `wanted` DISABLE KEYS */;
/*!40000 ALTER TABLE `wanted` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-14 11:29:23
